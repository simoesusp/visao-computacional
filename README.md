# Manuel OpenCV

## Instalation

### Repositorio
- https://github.com/opencv/opencv
- git clone https://github.com/opencv/opencv.git

### Instalation
- Installing required packages and tools
  - sudo apt install -y g++ cmake make git libgtk2.0-dev pkg-config
  - Nao precisei instalar Nada... Ja'  tinha tudo isso funcionando no meu ARK

- Na Raiz de onde clonou, tem que criar uma pasta 'build'

- Dentro da pasta 'build' digitar:
  - cmake ../opencv
  - make -j4
  - sudo make install

- Se der tudo certo, a library vai ficar em:
  - /usr/local/include/opencv4

### Testando ...
- Criar um programa chamado DisplayImage.cpp
```
	#include <opencv2/opencv.hpp>
	#include <stdio.h>
	using namespace cv;
	int main(int argc, char** argv)
	{
		if (argc != 2) {
			printf("usage: DisplayImage.out <Image_Path>\n");
			return -1;
		}
		Mat image;
		image = imread(argv[1], 1);
		if (!image.data) {
			printf("No image data \n");
			return -1;
		}
		namedWindow("Display Image", WINDOW_AUTOSIZE);
		imshow("Display Image", image);
		waitKey(0);
		return 0;
	}
```
- Create a CMakeLists.txt file:

	cmake_minimum_required(VERSION 2.8)
	project( DisplayImage )
	find_package( OpenCV REQUIRED )
	include_directories( ${OpenCV_INCLUDE_DIRS} )
	add_executable( DisplayImage DisplayImage.cpp )
	target_link_libraries( DisplayImage ${OpenCV_LIBS} )

- generate build files using cmake

  - cmake .

- build the program:
  - make

- Finalmente - teste o programa:
  - ./DisplayImage path_of_the_image
  -ex.: ./DisplayImage lena.jpg

### Tutorials OpenCV
- https://docs.opencv.org/4.x/d9/df8/tutorial_root.html

### Mat -> a matriz de imagens do opencv
- https://docs.opencv.org/4.x/d6/d6d/tutorial_mat_the_basic_image_container.html

### Funcoes para 'Image Processing'
- https://docs.opencv.org/4.x/d7/da8/tutorial_table_of_content_imgproc.html 

### Tutorial CMake
- https://cmake.org/cmake/help/latest/guide/tutorial/index.html

